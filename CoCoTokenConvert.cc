#include<stdio.h>
#include<ctype.h>

#include"MC10.h"

unsigned char ConvertOne(unsigned char in);
unsigned char ConvertTwo(unsigned char in);

unsigned char ConvertOne(unsigned char in)
{
	int x;
	unsigned char CoCoToken[47] = {128,129,130,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,
									148,149,150,151,152,155,156,157,158,160,162,163,164,165,167,168,169,170,
									171,172,173,174,175,176,177,178,179,180,186};

	unsigned char token[47] = {128,130,131,132,133,134,135,136,137,138,139,140,142,143,144,145,146,147,148,
								149,150,151,152,153,155,156,157,158,159,160,161,162,163,164,165,166,167,168,
								169,170,171,172,173,174,175,176,141};

	for(x=0;x<47;x++)
		if(in==CoCoToken[x])
			break;

	//is the character valid?
	if( (x==47)&&(!isalnum(in)) && (!isspace(in)) && 
		(in!='!')&&(in!='@')&&(in!='#')&&(in!='$')&&(in!='%')&&(in!='"')&&
		(in!=',')&&(in!=';')&&(in!='[')&&(in!=']')&&(in!='(')&&(in!=')')&&
		(in!='.')&&(in!='=')&&(in!='*')&&(in!=':')&&(in!='&')&&(in!='\'')&&
		(in!='-')&&(in!='+')&&(in!='/')&&(in!='?')&&(in!='<')&&(in!='>')		
		)
		printf("Unknown character %d\n",in);

	if(x==47)
		return in;
	else
		return token[x];

}//ConvertOne

unsigned char ConvertTwo(unsigned char in)
{
	int x;
	
	unsigned char tmp;
	unsigned char CoCotokens[23] = {128,129,130,131,132,133,134,135,136,138,139,142,143,144,145,146,147,
										149,150,151,153,155,157};
	unsigned char tokens[23] = {177,178,179,180,181,185,188,189,190,192,193,194,195,196,197,199,200,186,
									187,184,183,182,198};

	//replace the two byte 0xff+X token with replacement and a space
	for(x=0;x<23;x++)
		if(in==CoCotokens[x])
			break;

	if(x==23)
	{
		printf("Couldn't convert 255+%d, padding with spaces\n",in);
		return in;
	}
	else
		return tokens[x];

}//ConvertTwo

void MC10base::CoCoTokenConvert(void)
{
	unsigned char tmpc;
	unsigned short finish, start, x;

	printf("Converting BASIC tokens..");

	start=(Memory[0x93]<<8)+Memory[0x94];
	finish=(Memory[0x95]<<8)+Memory[0x96];

	//convert data
	for(x=start;x<=finish;)
	{
		//first skip the four header bytes and ignore
		x+=4;

		//now, read the line
		for(;;)
		{
			//is it the end of the line?
			if(Memory[x]==0)
			{
				x++;
				break;
			}//if
				
			if(Memory[x]!=0xff)
				Memory[x]=ConvertOne(Memory[x]);
			else
			{
				Memory[x]=ConvertTwo(Memory[x+1]);
				Memory[++x]=' ';
			}//if

			x++;
		}//for
	}//for

	printf("Done\n");
}//CoCoTokenConvert



