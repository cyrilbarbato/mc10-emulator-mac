#include<stdio.h>
#include<unistd.h>
#include<ctype.h>

#include"MC10.h"

/*
//filetypes
#define BASIC 0x00
#define DATA 0x01
#define ML 0x02

//datatypes
#define BINARY 0x00
#define ASCII 0xff

//gap types
#define CONTINUOUS 0x00
#define GAPS 0xff
*/
//WriteCassette(in,out,BASIC,BINARY,0x0000,"FNORD   ");

void MC10base::WriteCassette(int mode, int dataMode, unsigned short startAddress, 
					unsigned short loadAddress, unsigned short endAddress)
{
	FILE *out;
	int x, cnt, pos=startAddress;
	char outfile[50], filename[50];
	unsigned char tmpc,checksum;
	unsigned char blocktype, datalength, filetype=mode, datatype=dataMode, gapflag=CONTINUOUS;
	unsigned char tmpBuffer[255];

	//get filenames
	printf("Enter in cassette file to write out to: ");
	gets(outfile);
	printf("Enter in program name to save: ");
	gets(filename);

	for(x=0;x<8;x++)
		filename[x]=toupper(filename[x]);
	
	if(!(out=fopen(outfile,"ab")))
	{
		printf("Couldn't open file '%s' for writing, exiting\n",outfile);
		return;
	}//if

	if( (datatype!=BINARY)&&(datatype!=ASCII) )
	{
		printf("Invalid data mode %.2x, exiting\n",datatype);
		return;
	}//if

	/*
	//get start and load addresses
	switch(mode)
	{
		case BASIC:
		case DATA:
			startAddress=0x00;
			loadAddress=0x00;
			break;		
		case ML:
			break;
		default:
			printf("Unknown file mode %.2x! Exiting\n",mode);
			return;
			break;
	}//switch
	*/

	//write out leader
	tmpc=0x55;
	for(x=0;x<128;x++)
		fwrite(&tmpc,1,1,out);

	//write filename block
	blocktype=0x00;

	tmpc=0x55;
	fwrite(&tmpc,1,1,out);
	tmpc=0x3c;
	fwrite(&tmpc,1,1,out);

	fwrite(&blocktype,1,1,out);
	datalength=0x0f;
	fwrite(&datalength,1,1,out);

	checksum=datalength;	
	checksum+=blocktype;

	for(x=0;x<8;x++)
		tmpBuffer[x]=filename[x];
	
	tmpBuffer[8]=filetype;
	tmpBuffer[9]=datatype;
	tmpBuffer[10]=gapflag;
	tmpBuffer[11]=startAddress<<8;
	tmpBuffer[12]=startAddress&0xff;
	tmpBuffer[13]=loadAddress<<8;
	tmpBuffer[14]=loadAddress&0xff;
	
	for(x=0;x<15;x++)
	{
		fwrite(&tmpBuffer[x],1,1,out);
		checksum+=tmpBuffer[x];
	}//for

	fwrite(&checksum,1,1,out);
	tmpc=0x55;
	fwrite(&tmpc,1,1,out);
	
	//write out second leader
	tmpc=0x55;
	for(x=0;x<128;x++)
		fwrite(&tmpc,1,1,out);

	//write out data blocks
	cnt=0;
	while(pos<endAddress)
	{
		tmpBuffer[cnt++]=Memory[pos++];

		if(cnt==255)
		{
			cnt=0;
			
			//flush out buffer
			tmpc=0x55;
			fwrite(&tmpc,1,1,out);
			tmpc=0x3c;
			fwrite(&tmpc,1,1,out);
			tmpc=0x01;
			fwrite(&tmpc,1,1,out);
			checksum=0x01;
			tmpc=0xff;
			fwrite(&tmpc,1,1,out);
			checksum+=0xff;
			for(x=0;x<255;x++)
			{
				fwrite(&tmpBuffer[x],1,1,out);
				checksum+=tmpBuffer[x];
			}//for
			fwrite(&checksum,1,1,out);
			tmpc=0x55;
			fwrite(&tmpc,1,1,out);
		}//if
	}//if

	//flush out buffer
	tmpc=0x55;
	fwrite(&tmpc,1,1,out);
	tmpc=0x3c;
	fwrite(&tmpc,1,1,out);
	tmpc=0x01;
	fwrite(&tmpc,1,1,out);
	checksum=0x01;
	tmpc=cnt;
	fwrite(&tmpc,1,1,out);
	checksum+=cnt;
	for(x=0;x<cnt;x++)
	{
		fwrite(&tmpBuffer[x],1,1,out);
		checksum+=tmpBuffer[x];
	}//for
	fwrite(&checksum,1,1,out);
	tmpc=0x55;
	fwrite(&tmpc,1,1,out);

	//write out EOF blocks
	tmpc=0x55;
	fwrite(&tmpc,1,1,out);
	tmpc=0x3c;
	fwrite(&tmpc,1,1,out);
	tmpc=0xff;
	fwrite(&tmpc,1,1,out);
	tmpc=0x00;
	fwrite(&tmpc,1,1,out);
	tmpc=0xff;
	fwrite(&tmpc,1,1,out);
	tmpc=0x55;
	fwrite(&tmpc,1,1,out);

	fclose(out);
	
}//WriteCassette
